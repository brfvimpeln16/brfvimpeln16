Styrelsen
=========

Ordförande
----------
Sabine Finell, 16 A

Vice Ordförande
---------------
Christian Häggström, 16 B

Kassör
------
Jeanette Rudels, 16 C

Ledamöter
---------
Christoffer Van Woensel, 16 C<br>
Michael Örekärr, 16 C

Suppleanter
-----------
Raman Atroshi, 16 C<br>
Eldijana Huremovic, 16 A

Internrevisor
-------------
Ulla Forsman, 16 C<br>

Valberedning
------------
Michael Stridsberg, 16 A<br>
Victor Ramos, 16 A

Kontakt med styrelsen
=====================
Vill du komma i kontakt med föreningens styrelse kan du lämna
skriftliga meddelande. Om du vill ha svar måste det finnas
kontaktuppgifter (namn, lägenhetsnummer, e-post adress)

Post till styrelsen
-------------------
I B-porten finns en postlåda där du kan lägga
meddelanden till styrelsen.

Postadressen är:
```
Brf Vimpeln 16
Klippgatan 16B
171 47 Solna
```

E-post till styrelsen
---------------------
Mejl till styrelsen läses regelbundet, ofta varannan dag.
E-post <a href="mailto:styrelsen@vimpeln16.se">styrelsen@vimpeln16.se</a>

![](bilder/styrelse.jpg)
