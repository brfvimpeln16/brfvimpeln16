Vårt ansvar för miljön
======================
I vår förening tar vi ansvar för miljön, både
närmiljön och den globala.

Återvinningsstationen Skytteholmsvägen
--------------------------------------
I kommunens återvinningsstation vid Klippgatan - Skytteholmsvägen finns
behållare för glas, kartong,
tidningar, hårdplast, plåtförpackningar och batterier.

Matavfall
---------
lämnar du i vårt miljöhus vid gatan. Info om vad som
räknas som matavfall och
vilken påse som ska användas finns i miljöhuset
eller på Solna Stads hemsida.

Hushållssopor
-------------
(vanliga soppåsen) lämnar
du i vårt miljöhus vid gatan. Tänk på att alla hushållssopor ska
vara väl förslutna.


Tidningspapper
-------------
kan du lämna i vårt miljöhus vid gatan. Sortera
bort det som inte är tidningar.

Grovsopor, miljöfarligt avfall och elektronik
---------------------------------------------
är avfall
som är för stort eller olämpligt att lämna i
soppåsen. Exempel på detta är trasiga möbler,
husgeråd, färgburkar, emballage eller gamla cyklar.

Detta ska lämnas till miljöbilen som Solna Stad ställer
upp varje helgfri torsdag 17:00-17:30 Solnaplan, Solna Centrum.
Här kan du lämna grovavfall i storlek som du kan bära
samt farligt avfall.


Mer om återvinning
==================

Matavfall och biogasframställning
---------------------------------

Läs mer om matavfallshantering på
[Solna stads avfallssida](https://www.solna.se/boende--miljo/avfall-och-atervinning/matavfall).

Miljöfarligt avfall
-------------------
[Till Solnas miljösida](https://www.solna.se/boende--miljo/avfall-och-atervinning/farligt-avfall-och-elavfall).

Sörabs återvinningsstationer
----------------------------
Möbler, elektronikskrot, byggavfall och allt annat du vill
bli av med kan lämnas hos [Sörabs återvinningsstationer](https://www.sorab.se/).

Apoteket
--------
Överbliven medicin lämnas i apoteket i Solna Centrum.
[Till apotekets återvinningsinformation](https://www.apoteket.se/privatpersoner/kundservice/sidor/apoteketcontents_overblivnalakemedel_overblivenmedicin_paverkamiljonpositivt.aspx).

![](bilder/recycle.gif)
