Föreningsinformation
====================

* <a href="regler.html">Medlemsinformation och Ordningsregler</a>
* <a href="dokument/Stadgar_20241113.pdf">Stadgar för Brf Vimpeln 16</a>
* <a href="dokument/Brandskyddspolicy_Vimpeln16.pdf">Brandskyddspolicy</a>
* <a href="dokument/energideklaration_2019.pdf">Energideklaration 16A-16B-16C</a>
* <a href="dokument/UH-plan%202018-2048.pdf">Underhållsplan 2018-2048</a>
* <a href="dokument/arsredovisning_2023.pdf">Årsredovisning 2023 brf Vimpeln16</a>
* <a href="dokument/revisionsberattelse_2023.pdf">Revisionsberättelse 2023 brf Vimpeln16</a>
* <a href="dokument/arsredovisning_2022.pdf">Årsredovisning 2022 brf Vimpeln16</a>
* <a href="dokument/Prot_20241113.pdf">Protokoll extra föreningsstämma 2024-11-13</a>
* <a href="dokument/Prot_20240529.pdf">Protokoll föreningsstämma 2024-05-29</a>
* <a href="dokument/Prot_20240118.pdf">Protokoll extra föreningsstämma 2024-01-18</a>
* <a href="dokument/Prot_20230529.pdf">Protokoll föreningsstämma 2023-05-29</a>
* <a href="dokument/Prot_20220530.pdf">Protokoll föreningsstämma 2022-05-30</a>


Föreningens lägenheter och lokaler
----------------------------------
Föreningen har 100 lägenheter fördelat på:

| Storlek | Antal | A-port | B-port | C-port |
|:-------:|:-----:|:------:|:------:|:------:|
| 1 r&k   | 17 st | [Skiss](dokument/lag1aa.htm) | | [Skiss](dokument/lag1ac.htm) |
| 2 r&k   | 24 st | [Skiss](dokument/lag2aa.htm) | | [Skiss](dokument/lag2ac.htm) [Skiss](dokument/lag2acb.htm) |
| 3 r&k   | 43 st | [Skiss](dokument/lag3aa.htm) [Skiss](dokument/lag3aab.htm) | [Skiss](dokument/lag3ab.htm) [Skiss](dokument/lag3abb.htm) | [Skiss](dokument/lag3ac.htm) |
| 4 r&k   | 16 st | [Skiss](dokument/lag4aa.htm) | | [Skiss](dokument/lag4ac.htm)

Till det kommer 2 lokaler för uthyrning.

Föreningen har även 53 parkeringsplatser på egen mark, 24 utomhus
och 28 i garage.

Anvisningar och regler
----------------------
* <a href="dokument/efter_OVK2.pdf">Anvisning för ventilationssystem</a>
* <a href="dokument/koksflakt.htm">Regler för montering av köksfläkt</a>
* <a href="dokument/balkong.htm">Regler för underhåll av balkonginglasningar</a>

Allmän information
------------------

<strong>Organisationsnummer</strong> till föreningen är 769604-5371.

<strong>Autogiro</strong> för
avgift/hyra ansöks eller avslutas genom en blankett som fås av
styrelsen.

<strong>Inbetalningskort</strong> för
avgift/hyra utlämnas varje kvartal. Om betalning sker på annat sätt
måste namn och den åttasiffriga koden anges så att styrelsen vet
från vem inbetalningen kommer.

<strong>Fixardagar</strong> infaller
normalt på
- sista onsdagen innan valborg/1:a maj
- sista söndagen i oktober

Då hjälps vi åt att göra fint i vårt
hus och trädgård. Vi avslutar
med en lättare förtäring.

![](bilder/stadgar.gif)
