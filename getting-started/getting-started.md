---
title: Hur arbetar jag med hemsidan?
layout: guide
---
Hur arbetar jag med hemsidan?
=============================

Klicka på "föreslå ändringar till denna sida" när du hittar något som är fel. Du ombedes då skapa en användare. Tryck på "Register now".

![Alt text](image-1.png)

![Alt text](image-2.png)

Fyll i formuläret som visas. Du kan använda pseudonym men mail-addressen måste vara rätt då de skickar ett bekräftelsemail dit.

![](image-3.png)

![Alt text](image-4.png)

Klipp-och-klistra in koden i mailet du ska ha fått, och klicka "Next".


![Alt text](image-5.png)

Välj "Other", "Just me" och "Join a project". Klicka sedan på  "Continue".

![Alt text](image-7.png)

Nu får du upp sidan du försökte redigera i gitlabs miljö. Tryck på Edit -> Edit singe file, och sedan Fork.

![](image-6.png)
![Alt text](image-8.png)

Nu kan du äntligen börja redigera texten!

![Alt text](image-9.png)

I detta exempel ändrar vi 30 minuter till 15 minuter.

![](image-10.png)

Du kan se hur dina ändringar visas i HTML genom att klicka på "Preview". Detta kan vara bra om du ändrat i tabeller eller annan uppmärkning.

![Alt text](image-11.png)

Skriv en motivering till din ändring, och klicka på "Commit changes".

![Alt text](image-12.png)

Ytterligare en bekräftelse-sida, klicka på "Create merge request".

![Alt text](image-13.png)

Sådär! Nu är ändringen inskickad och hemsidesgruppen får mail om det. För att se borttagen och tillagd text, klicka på "Changes (1)".

![Alt text](image-14.png)

![Alt text](image-15.png)

Man kan diskutera en specifik ändring genom att trycka på pratbubblan.

Du kan alltid komma tillbaka till ditt och andras ändringsförslag genom att klicka på "Merge Requests" i vänsterspalten:

![Alt text](image-16.png)

Lycka till önskar Christian!
