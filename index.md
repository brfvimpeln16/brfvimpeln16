Välkommen till brf Vimpeln 16 i Solna
=====================================

Här kan du som bor i föreningen och även besökare
som vill lära känna
vår förening finna information om vad som finns
och hur det fungerar. Du kan läsa notiser, styrelseutskick och
information om olika funktioner.

Information om vår förening
---------------------------
Vår förening är belägen på Klippgatan 16
i Solna, i stadsdelen Skytteholm nära Solna Centrum.

När du köper en bostadsrätt och flyttar till vår
förening, blir du delägare i föreningen
som tillsammans äger
fastigheten. Det innebär att du kan vara med och påverka
vad som skall ske. Tillsammans är vi varandras boendemiljö.

Som medlem kan du bidra till föreningens
utveckling och stabilitet genom att aktivt delta i styrelsearbete,
valberedning och arbetsgrupper. Ju fler medlemmar
som är aktiva, desto bättre blir vårt gemensamma resultat

En gång om året håller vi en årsstämma.
Då väljs en styrelse av de som bor i föreningen.
Styrelsens uppgift är
att förvalta föreningens egendom, vilket bland annat innebär
att se till att reparationer och underhåll genomförs enligt
den fastställda underhållsplanen. Styrelsen skall även
se till att föreningens stadgar efterlevs och att årsstämmans
beslut genomförs.

![](bilder/banner_tom.gif)

Ansvarig utgivare: <a href="styrelsen.html">Styrelsen</a>.
