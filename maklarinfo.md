Fakta / mäklarinformation för Brf Vimpeln16
===========================================

Förening: **BRF Vimpeln16 i Solna (769604-5371)**

Hemsida: http://www.vimpeln16.se stadgar, årsredovisning,
energideklaration m.m.

E-post: styrelsen@vimpeln16.se

Post: Klippgatan 16B, 17147 Solna

**Allmänt om föreningen**

- **Vad ingår i avgiften?**

  - Värme, vatten, kabel-tv (digitalt
    grundutbud), Gemensamt elavtal: enskild förbrukning per
    lägenhet tillkommer

- **Ingår bostadsrättstilläggsförsäkring i avgiften?**

  - Ja, via If

- **Tar föreningen ut överlåtelseavgift – vem betalar den i så fall?**

  - Ja, köpare. Överlåtelseavgift, 1433 kr. Vid försäljning kommer delar
    av styrelsen att träffa köparen, innan vi godkänner medlemskapet.
    Köpare och säljare gör själva upp om förskottsbetald el vid tillträdet.

- **Tar föreningen ut pantsättningsavgift?**

  - Ja, 573 kr.

- **Antal lägenheter i föreningen?**

  - 100 lägenheter, varav 96 brf och 4 hr samt 2 affärslokaler. Total yta
  för bostads- och hyresrätter är 6 919 m². Total yta för affärslokaler är 160 m².

- **Är föreningen äkta eller oäkta?**

  - Äkta.

- **Godkänns juridisk person?**

  - Nej.

- **Godkänner föreningen delat ägande?**

  - Ja.

- **Krav på minsta ägarandel?**

  - 10%

**Avgiftsförändringar/renoveringar**

- **Har föreningen några avgiftsförändringar planerade?**

  - Nej. Senaste höjningen på 5 % genomfördes 1 april 2024.

- **Kommande/pågående renoveringar vi bör känna till?**

  - Inga för tillfället, men inom kommande två år ska fasaden och
    balkonger ses över.

- **Gjorda renoveringar?**

  - Ombyggnad 1993, fasad och balkonger.

  - 2008/2009 stambyte utfört samt nya badrum.

  - 2008/2009 byte av all el: jordfelsbrytare. Tillval: en del har trefas.

  - 2010 hissar till EU- standard.

  - 2012 målning av trapphus och entré.

  - 2014 Nytt miljöhus.

  - 2017 renovering av garage, ramp till parkering och trappa.

  - Radonmätning.

  - 2021 renovering av tvättstuga, byte till ny maskinpark.

  - 2023 Stamspolning.

  - 2023 målning av taket.

  - 2024 OVK.

**Fastigheten**

- **Byggår**

  - 1964/65, inflyttning 1965.

- **Uppvärmning**

  - Fjärrvärme.

- **Ventilation**

  - Mekanisk fläkt luft.

- **Godkänd köksfläkt**

  - Kolfilter.

- **Finns det hiss?**

  - Ja.

- **Utförs trappstädning av städfirma?**

  - Ja.

- **Äger föreningen marken?**

  - Ja. Fastighetsbeteckning: Vimpeln 12.

- **När förvärvade föreningen fastigheten?**

  - 2001.

**Gemensamma utrymmen**

- **Vad har föreningen för gemensamma utrymmen?**

  - Barnvagnsrum.

  - Cykelförråd.

  - Tvättstuga.

  - Gästlägenhet som kan hyras av medlemmar för 450–500 kronor/natt.

  - Mötesrum för 10 personer.

  - Verkstadsrum.

  - Grillplats i trädgården.

- **Om gemensam tvättstuga finns – vad är tvättstugan utrustad med?**

  - Tvättstugan består av 2 tvättstugor. De är utrustade med 3
 tvättmaskiner, 1 torkskåp samt 1 torktumlare samt varsitt torkrum.
Tvättstugan bokas med en tvätt kolv märkt med lägenhetens nummer.

- **Finns det extra förrådsutrymmen att hyra?**

  - Extra förrådsutrymme kan hyras i mån av plats. För närvarande kö.

**TV/Bredband**

- **Ingår det i avgiften eller tecknas det enskilt av medlem och vilka
  leverantörer kan man använda sig utav?**

- Bredband: leverantör Ownit, obligatorisk anslutning och avgift, f.n.
  160kr/månad Kabel-TV: basutbud via Telenor ingår i avgiften.

- **Parkering**

- **Har föreningen tillgång till några parkerings- eller
  garageplatser?**

- Parkering: 24 platser utomhus och 28 garageplatser. Ansökan till
  styrelsen. Utomhus kostar 300 kr, garage 660 kr.

- **Hur lång kö är det till platserna?**

  - Det är f.n. ingen kö till platserna.

- **Har föreningen tillgång till parkeringsplatser för MC/moped?**

  - Det finns några MC-platser i garaget. De kostar 300kr.

- **Hur lång kö är det till mc-platserna?**

  - Det är f.n. ingen kö till mc-platserna.

**Medlemskap**

- **Vart ska vi skicka ansökan om medlemskap när affären är avslutad?**

  - Alla kontraktsärenden hanteras av Princip Redovisning, Valhallavägen
    165, 115 53 Stockholm Tel 070-739 56 50, 0703- 39 99 09

- Senast uppdaterad 2024-10-20.
