Välkommen till bostadsrättsföreningen Vimpeln 16 i Solna
--------------------------------------------------------

Vår bostadsrättsförening med namnet Vimpeln 16 äger fastigheten med
beteckningen Kv. Vimpeln 12. Fastigheten byggdes 1963. Ombildning till
bostadsrättsförening gjordes våren 2000. Fastigheten består av:

* 100 lägenheter
* 2 uthyrningslokaler till företag
* 24 parkeringsplatser
* 28 garageplatser

I denna folder finner ni information om föreningen och fastighetens
skötsel, daterad 2024-05-21. En uppdaterad version av denna folder
finns på föreningens hemsida, där det även finns mer
information. Adress är www.vimpeln16.se

Medlemsinformation
==================

## Kontakt med styrelsen
Styrelsen nås lättast via mejl. Det finns även en telefon som bemannas tisdagkvällar, nummer 076-607 29 45. SMS eller lämna meddelanden går bra övrig tid.
Mejladress: styrelsen@vimpeln16.se


## Valberedning

I föreningen finns en valberedning som består av valda representanter ur föreningen.

Valberedningen är medlemmarnas viktigaste instrument för att skapa och bibehålla en fungerande styrelse som på ett kvalificerat och tryggt sätt kan hantera föreningens verksamhet framöver. Ofta är det valberedningen som tar initiativ till ett nödvändigt förändringsarbete. Valberedningen har alltså en nyckelfunktion och dess uppgift är bl.a. att från den stund den utses, fram till nästa årsstämma, analysera hur den sittande styrelsen arbetar.

Valberedningens arbete pågår därmed parallellt med styrelsens arbete under hela det år den är vald för.

Om du är intresserad av styrelsearbete, tveka inte att kontakta valberedningen när som helst under året. Lägg ett brev i föreningens brevlåda (B-porten), adresserad till valberedningen. Information om valberedningens arbete kommer som en separat punkt.

## Föreningsstämma
En gång per år hålls ordinarie föreningsstämma. Då behandlas val av styrelse, revisorer och valberedning. Föreningens bokslut presenteras och medlemmarnas motioner (förslag) behandlas. För att en motion skall tas upp på stämman måste den vara styrelsen tillhanda senast en månad innan stämman. Stämman hålls normalt i slutet av maj månad. Kallelse med dokumentation går ut till alla medlemmar två till fyra veckor innan stämman genomförs. Se även föreningens stadgar.

## Årsredovisning och stadgar
Föreningens årsredovisning och stadgar finns på hemsidan eller går att få via styrelsen.

## Avgiften
Att bo och vara medlem i en bostadsrättsförening innebär att man tillsammans med andra medlemmar i föreningen äger det hus och den mark som föreningen består av. Alla kostnader och utgifter som föreningen har för hus och gård fördelas ut på varje enskild bostadsrätt genom en avgift som fastställs av föreningens styrelse. Det finns således aldrig någon annan som betalar, utan alla kostnader och utgifter betalas gemensamt av oss medlemmar.

Ju lägre kostnader föreningen har, desto lägre avgift för respektive bostadsrättshavare. Det är därför viktigt att vi medlemmar är aktsamma om vår gemensamma utrustning som t.ex. tvättmaskiner, hissar, portar och gårdar.

Betrakta huset och gården som din egen – för det är precis vad det är.

## Fastighetsskötsel och felanmälan och akuta skador
Den dagliga skötseln av fastigheten utförs idag av Driftia. På hemsidan och i portarna finns anslag hur man kontaktar dem, och felanmälan skall alltid göras till Driftia.
Om ditt ärende tas om hand på ett sätt du inte är nöjd med bör du kontakta styrelsen.
Vid problem med hissar, ring direkt till det företag föreningen anlitar, telefonnummer finns vid hissen. Vid skador eller utryckningar som en medlem själv förorsakat ska medlemmen betala fakturan, till exempel om nycklar tappas ner i hisschaktet eller att en spegel krossas pga. oaktsamhet.

## Bredband
Vår förening äger fastighetsnätet och köper anslutning genom leverantören Ownit. Föreningen har avtalat om en gruppanslutning för att få en förmånlig anslutningsavgift för medlemmarna, och bredband och avgift är obligatorisk för alla lägenheter. Om du är intresserad av bredband eller behöver information om din anslutning kan du kontakta Ownit direkt.
Mer info om bredband finns på hemsidan www.vimpeln16.se

För aktivering av bredband och andra tjänster, gå till www.ownit.se. Klicka på Bredband och välj din gatuadress och lägenhetsnummer. Koppla in en dator eller router till anslutningsdosan/skåpet ovanför din entrédörr. Den utrustning du kopplar in till dosan ska vara inställd för att hämta ”Dynamisk IP-adress”.

Om du aktiverat bredbandet har du också ingått ett avtal med vår leverantör, och är därmed juridiskt ansvarig för hur ditt bredband används. Av det skälet är det viktigt att du säger upp ditt bredbandsabonnemang till vår leverantör om du flyttar från vår förening.


## Kabel-TV
I föreningen finns kabel-TV. I varje lägenhet finns ett eller flera uttag. Om du önskar ett större utbud utöver bas-utbudet som ingår i månadsavgiften, finns mer information på hemsidan om föreningens nuvarande leverantör Telenor.

## Föreningens intressegrupper
I föreningen finns intressegrupper som har hand om olika områden, som exempelvis trädgårdsgrupp, trivselgrupp. Vill du vara med i en grupp eller skapa en ny så kan du ta kontakt med styrelsen för mer information.
Om du är kunnig eller intresserad av något som föreningen kan ha nytta av (t.ex. IT) så kan du ta kontakt med styrelsen.

## Fixardagar
På vår och höst arrangeras gemensamma fixardagar då alla samlas för några timmars arbete och trevlig samvaro. Arbetsuppgifterna anslås och delas ut vid varje träff beroende på vad som behöver göras. Här lär man känna sina grannar under trevliga förhållanden. Några veckor innan fixardagarna annonseras de ut i portarna och på hemsidan. Fixardagarna är fasta och infaller vanligen:
Kvällstid sista onsdagen innan Valborg/1 maj och dagtid sista söndagen i oktober. 


## Att bo i en bostadsrätt
Skillnaden mellan att bo i en bostadsrätt och en hyresrätt är ganska stor. När du köper en lägenhet, köper du inte den fysiska bostaden utan blir delägare i fastigheten. Man köper sig rätten att bo på ett antal kvadratmeter som ägs av en ekonomisk förening. Du får därmed rätten att nyttja lägenheten som bostad men får samtidigt också ett ansvar gentemot hela bostadsrättsföreningen. Detta innebär inte bara en skyldighet att ta väl hand om den egna bostaden utan också att tillsammans med alla andra medlemmar ta ansvar för de gemensamma utrymmena i huset. Vad du köper är en andel av föreningen - ungefär som en aktie i ett företag.

Ansvarsfördelningen mellan dig och föreningen finns reglerad i föreningens stadgar.
Förenklat gäller att du har ansvar för bostaden du bor i och att föreningen ansvarar för själva fastigheten, exempelvis skötsel och underhåll av fasad, förråd, tvättstugor, trapphus, hissar och gården.

## Renovering och ombyggnad
Vid ombyggnad av lägenheten, som att ta ner/sätta upp en vägg eller sätta in diskmaskin, måste styrelsen godkänna denna ombyggnad. Vid renovering av balkonger är endast tillåtet att måla tak och väggar. Det är förbjudet att borra i ytterväggen då den innehåller asbest. Se även föreningens stadgar.

## Årsavgift
Som bostadsrättshavare betalar du en årsavgift. Den delas upp på tolv månadsinbetalningar och avgiften skall vara föreningen tillhanda, via ekonomisk förvaltare, senast förfallodagen varje månad. Ansvaret ligger kvar hos medlemmen även vid andrahandsuthyrning till annan part. Se även föreningens stadgar.

## Hemförsäkring
BRF Vimpeln 16 har en kollektiv bostadsrättsförsäkring som gäller för alla medlemmar.
Det är dock viktigt att alla har en hemförsäkring men ni behöver alltså inte ha det bostadsrättstillägg som ofta annars är vanligt att försäkringsbolagen rekommenderar.

## Andrahandsuthyrning
Tänk på att uthyrning via Airbnb eller en annan liknande tjänst faller in under samma regelverk som alla andra former av andrahandsupplåtelser. Det är viktigt att vara medveten om att varje ny uthyrning ses som en självständig andrahandsupplåtelse som kräver tillstånd av styrelsen. Ett tillstånd från styrelsen att upplåta sin lägenhet i andra hand går således inte att använda för flera korta uthyrningar. Se även föreningens stadgar.


## Överlåtelse av bostaden
Då köpet är klart ska styrelsen förses med två exemplar av köpehandlingen samt ansökan om medlemskap. Om inga hinder för medlemskap anses föreligga godkännes medlemmen.
Frågor kring pantsättning och dylikt hänvisas till föreningens ekonomiske förvaltare info@principredovisning.se


## Tvättstugan
Tvättstugan ligger i källargången mellan B- och C-porten. Bokning av tid görs på en tavla för tvättider med hjälp av bokningscylinder. Kontakta styrelsen för att anskaffa ny cylinder till självkostnadspris om den saknas eller har gått sönder.
Tvättstugan har öppet:

* Vardagar mellan kl. 07.00 och 22.00,
* Lördagar och söndagar mellan kl. 10.00 och 22.00.

Övrig tid är maskinerna strömlösa via timer.
Bokat pass som inte påbörjats efter 15 minuter får övertas av annan medlem.
Det är inte tillåtet att färga kläder i maskinerna. Mattor kan endast tvättas i maskiner med tydlig markering för sådan tvätt. Varje medlem som tvättat städar efter sin egen tvättid.
Skulle en maskin vara ur funktion, vänligen sätt en lapp på maskinen om felanmälan & datum då du kontaktade styrelsen. Torkrummen får tillträdas en timme efter att tvättiden startar och utnyttjas fram till en timme efter att tvättiden är slut.

## Ventilation och köksfläkt
Fläktar i bostaden skall vara av passiv typ då fastigheten har centralfläktsystem. Motordrivna fläktar får INTE anslutas direkt till ventilationsdosor och husets ventilationssystem. Vill du ha en sådan köksfläkt får den endast vara av typen med kolfilter. Den suger in köksluft, renar den i ett filter och släpper ut luften i köket igen.

## Balkonginglasning
Inglasning av balkong kräver byggnadslov. Kontakta styrelsen för att anmäla din önskan om inglasning. Det finns inte något generellt bygglov för fastigheten. Eventuell kostnad för sådant och andra kostnader för tillstånd som kan uppkomma står medlem för. Balkong som blir inglasad måste vara i enlighet med övriga i fastigheten.
Den inglasade balkongen skall underhållas med jämna mellanrum så att funktion och säkerhet hålls på en god nivå.
De juridiska aspekterna på detta är följande:
* Medlemmen ansvarar för och är underhållsskyldig för inglasningen av balkong. Kostnader för underhåll står medlemmen för.
* Det är fastighetsägarens (föreningens) ansvar att olyckor med nedfallande föremål från fasaden inte inträffar.
Det innebär att föreningen inte bekostar reparationer av inglasning, men att föreningen har besiktningsansvar för balkonger, och kan kräva att medlemmar antingen åtgärdar eller plockar ned exempelvis defekta inglasningar. Föreningen tar ut en kostnad av medlemmen för sådan besiktning. Besiktning ska utföras vart 5:e år.
* Färgval på väggar och markiser ska ske harmoniserat enligt övriga byggnaden. För väggar och tak, vit eller ljusgrå. Markiser ska vara ljusa, grå, vita eller randiga.
* Markiser får inte fästas i väggen på det sättet att de borras in i fasaden.

## Övernattningsrum
Föreningen har ett rum samt tillhörande dusch och toalett som medlemmarna kan hyra för korta perioder. Det finns 4 sängplatser. För matlagning finns kylskåp, mikrovågsugn och vattenkokare.
Städning efter avslutat boende ingår ej i hyran. Vid otillräcklig städning kommer en städavgift att debiteras den medlem som senast hyrt övernattningsrummet.

## Hobbyrum
Ett hobbyrum finns i källargången mellan A- och B-porten. Där finns möjlighet att snickra och måla. En liten uppsättning verktyg och maskiner finns att tillgå. Städa rummet efter dig och ta med avfallet du skapat. Bokningskalender finns på dörren och portnyckel passar.

## Gemensam El
Föreningen har ett gemensamt abonnemang för el, men med enskild avläsning av förbrukning.  Medlemmens egen förbrukning debiteras i efterhand på ordinarie avgiftsavier med tre månaders eftersläpning, i januari debiteras elen för september månad, osv.

Ordningsregler
==============

Det är viktigt att alla boende i föreningen läser föreningens medlemsinformation/ ordningsregler och stadgar samt förstår och följer dessa. Det är alltid den enskilde medlemmens ansvar att hålla sig uppdaterad och informerad om de regler som gäller boendet, i form av såväl lagar som stadgar och ordningsregler.
Föreningens ordningsregler och stadgar gäller inte bara dig som är bostadsrättshavare utan också de som tillhör ditt hushåll, är inneboende eller gäster, hyr i andra hand eller utför arbeten i lägenheten.

Brytande mot ordningsregler och stadgar kan medföra uppsägning av medlemskapet i föreningen.

## Allmänna brandskyddsregler
Som medlem är du skyldig att ha minst en fungerande brandvarnare i bostaden. 
Inom föreningen pågår löpande ett Systematiskt Brandskyddsarbete, SBA, och styrelsen har utsett en Brandskyddsansvarig som ronderar varje månad. Dennes anvisningar skall alltid följas. 
Med hänsyn till brandsäkerheten får inga lösa föremål placeras i trapphuset eller i andra allmänna utrymmen då de kan antändas eller försvåra utrymning. En dörrmatta får vara maximalt 3mm tjock.
Inga brandfarliga varor eller vätskor får förvaras i fastigheten. Se mer info på www.msb.se

Lösa brinnande föremål kan inom 5 - 10 minuter fylla trapphuset med rök som är så giftig att endast 3 andetag kan medföra medvetslöshet.
Lösa föremål som exempelvis barnvagnar och pulkor i allmänna utrymmen kommer att tas i förvar.
Ej efterfrågade föremål kommer att kasseras efter sex månader.
Boende får en lapp i brevlådan gällande omhändertagandet, om föremålet går att identifiera som en ägodel tillhörande en specifik lägenhet.
Grillning är inte tillåtet på balkongen med varken gasol- eller kolgrillar. Endast elgrill är tillåtet.
Raketer eller annan form av fyrverkerier får inte avfyras inom fastighetens område.

Oövervakad laddning eller användning av eluttagen i garaget kan
innebära brandrisk. Underhållsladdning av startbatterier får dock ske
i garaget.

## Säkerhet och grannsamverkan
När du passerar in eller ut genom porten, ta för vana att kontrollera att den går säkert i lås. Släpp inte in okända genom porten. Servicefolk och andra med legitima ärenden har egen nyckel eller portkod. Lämna inte vanligen låsta dörrar uppställda utan tillsyn. 

Meddela betrodd granne eller styrelsen om du är bortrest en månad eller mer och säkerställ att någon har åtkomst till lägenheten då du inte själv är tillgänglig. Detta då ett behov av tillträde kan uppstå exempelvis på grund av vattenskada.
Lämna om möjligt kontaktuppgifter med mobiltelefonnummer och även e-post till styrelsen.

## Parkering
Föreningen har 52 parkeringsplatser på egen mark, 24 utomhus och 28 i garage mellan Klippgatan 16 och 20. Utöver dessa finns 2 MC-platser i garaget.
Varje parkering har en målad ruta med platsens nummer i garaget.
Det är inte tillåtet att parkera någon annanstans på vår gård då det exempelvis hindrar service till föreningen så som sophämtning, brandkår mm.

Solna stad erbjuder boendeparkering längs Klippgatan. Detta innebär att det är lägre avgift för folkbokförda inom Solna. Se Solna stads hemsida för information och beställning av tillstånd till boendeparkering.

Kontakta Europark på 0771-401020 då bil står felparkerad på föreningens område.

Lastning och lossning får dock ske på föreningens mark.

I garaget, men även utomhus, skall du tänka på att:

* Fordonet får inte läcka olja, bränsle, kylvätska eller liknande. Om läckage upptäcks, lägg plast under fordonet i avvaktan på reparation.
* Förvaring av tillbehör för fordonet är ej tillåtet. 
* För motorcykel kan separat plats hyras i garaget.
* Vaxa, städa bilen och byta däck är tillåtet men inte tvätt av bil.

Var vaksam så att inga obehöriga släpps in i garaget.
Endast fordon som brukas regelbundet året runt av medlemmar eller hyresgäster har rätt till en garageplats. Fordonet skall rymmas inom anvisad plats. Om så inte är fallet har ägaren till fordonet TRE månader på sig, från skriftlig anmodan, mottagits att uppfylla parkeringsvillkoren. Om inte detta hörsammas anser föreningen att avtalet har upphört och avflyttning skall ske omgående.
Tillfällig avställning av fordonet får ske under en kortare tid, en gång per kalenderår. Styrelsen skall informeras om detta. Andrahandsuthyrning är inte tillåtet för parkeringsplats eller garageplats.

På p-däck och garage får endast personbilar klass I och motorcyklar stå, för nyskrivna kontrakt (Äldre, fortfarande giltiga kontrakt, har inte den begränsningen). Övriga fordon ställs på gatan.

Laddning av elmopeder får ske i garaget, max laddeffekt 350W.

## Regler för P-platskö
Hushåll har rätt till maximalt en parkeringsplats för bil i föreningen.
Det finns två separata köer för våra P-platser i garaget och utomhus.
Anmälan sker till styrelsen med lägenhetsnummer & namn.
Ni väljer själva om intresse finns att stå i båda köerna eller bara en.

## Barnvagn och cykelrum
Barnvagn/cykelrum är till för icke motordrivna åkdon, t.ex. pulkor går bra att förvara vintertid.  Vissa rum kommer man endast åt med en speciell nyckel som får kvitteras ut av styrelsen.
Styrelsen aviserar regelbundet om behovet av tillfällig märkning med namn & lägenhetsnummer på de cyklar/barnvagnar som förvaras i dessa utrymmen.
Omärkta cyklar/barnvagnar eller andra föremål kommer därefter att samlas ihop, förvaras i 6 månader och därefter avyttras.


Trivselregler
-------------

## Grannsämja och störningar
Då du bor i ett flerfamiljshus måste du acceptera ljud som hör till ett normalt liv, men ständigt pågående och allvarliga störningar behöver du inte tåla oavsett dag som natt. 
Normalt skall tystnad råda både inomhus och runt fastigheten 
mellan 22:00 -07:00. 
Planerar ni att reparera eller renovera i lägenheten skall ”bygg-frid” råda
mellan 19:30-08:00 (inga hammarslag eller borrning i betong).
Lördag-, söndag- eller helgmorgon råder byggfriden fram till klockan 10:00.
Om ni har planerat en tillställning eller reparation/renovering bör ni i god tid informera era grannar med info om bland annat tidpunkt, namn, lägenhetsnummer och hur man kommer i kontakt med er på telefon. Alternativet är att sätta upp lappar i våra allmänna utrymmen med information. Acceptansen brukar öka markant om man har blivit informerad och kan komma i kontakt med er på ett enkelt sätt.

Om du blir störd av höga ljud från en granne skall du kontakta den som stör och göra den uppmärksam på problemet. Ibland kanske grannen inte ens vet om att den stör. Vid upprepad störning bör du kontakta styrelsen.

## Rökning
Rökning är inte tillåtet i gemensamma utrymmen. Visa hänsyn mot icke-rökare och använd sunt förnuft, oavsett om du röker på gården, balkongen eller genom ett öppet fönster. Undvik att röka inom 8 meter från en port.
Det är inte tillåtet att slänga fimpar eller portionssnus på föreningens mark.

## Balkonger
Det är inte tillåtet att skaka eller piska mattor på balkongen, då det lätt yr in till din granne. Inga delar av blomlådor monteras utanför balkongräcket. Balkongen är ingen förvaringsplats, det ser trevligare ut om man inte staplar kartonger ute på balkongen.

## Husdjur och fågelmatning
Innehavare av husdjur är skyldiga att övervaka att djuren inte förorenar eller stör i fastigheten. Vissa djur i bostaden kräver tillstånd från kommunens hälsovårdsförvaltning och då även av styrelsen. Fågelmatning är inte tillåtet, varken på gården eller på balkongen, då frön och matrester riskerar locka till sig råttor och andra skadedjur.


## Hissar
Det finns 3 hissar i fastigheten som klarar ca 500 kg belastning. 
Det finns en knapp som fungerar som nöd-telefon i hissen. Den är kopplad till larmcentralen där du kan begära hjälp om hissen stannar.
Då du flyttar skrymmande saker med hjälp av hissarna ska filtar täcka väggarna för att skydda träpanelerna och speglarna från skador. 
Filtarna finns i varje ports källare och hängs upp i hissens takkrokar.

## Lägenhetsdörr / Säkerhetsdörr
Vid byte av dörr, ska dörrfodret målas enhetligt ( dörrfoder rödbrun, Kulör Alcro 863 Järnoxid/40 ) NCS 5040-Y70R glans 40%.
Dekorationer inför högtider bör inte sättas fast på väggarna, då vår stucco lustro-målning är känslig och svår att rengöra från tejprester.


## Avfallshantering

Förpackningar, glas, plast- och metallförpackningar, kartong, tidningar, kläder och batterier lämnar du på återvinningsstationen på Skytteholmsvägen 2. Återvinningsstationen drivs av FTI och du hjälper därmed till att minska föreningens kostnader.

Matavfall lämnar du i vårt miljöhus mellan Klippgatan 16 och 20. Info om vad som räknas som matavfall, vilken påse som ska användas mm finns i miljöhuset och på Solna Stads hemsida.

Hushållssopor (vanliga soppåsen) lämnas i vårt miljöhus. Tänk på att alla hushållssopor ska vara väl förslutna. I miljöhuset kan du även lämna småelektronik och lampor som kommunen hämtar.

Grovsopor, miljöfarligt avfall och elektronik är avfall som är för stort eller olämpligt att lämna i soppåsen. Exempel på detta är trasiga möbler, husgeråd, färgburkar, emballage eller gamla cyklar. Små grovsopor lämnas till Returpunkten i Solna Centrum, se https://www.sorab.se/hushall/vara-anlaggningar/returpunkt-solna-c/ för öppettider.

Läkemedel och medicin ska lämnas tillbaka till Apoteket.

Container brukar föreningen hyra in till fixardagarna. I containern kan du lämna skrymmande grovsopor exempelvis soffa, säng m.m. men ingen elektronik eller farligt avfall. För mer info, se anvisningar på aktuell inhyrd container.

Eluttagen i miljörummet ska endast användas för rengöring av miljörummet.

Valberedningens arbete
======================

Valberedningen har i grunden tre arbetsuppgifter;

1. Samordna förslag på medlemmar till olika förtroendeposter.
2. Fungera som en kontrollfunktion för att arbetet i styrelsen (etc) fungerar.
3. Löpande & på stämma informera medlemmarna om valberedningens arbete och resultat.

Valberedningen är en samordnande enhet och en mycket viktig del av föreningens verksamhet. Ju bättre valberedningen sköter sina uppgifter desto bättre förutsättningar har föreningen i stort. Så se till att välja rätt personer till valberedningen. De måste vara engagerade och intresserade av sin förening.
           Vilka poster ska valberedningen hitta personer till?
Valberedningen ska föreslå ledamöter till följande poster inom föreningen och presentera dem på stämman;

* Styrelsen
* Suppleanter till styrelsen
* Internrevisor och Suppleant till internrevisorn
* Valberedning (alltså föreslå ersättare till sig själva)
* Olika projekt som medlemmar ska ingå i (projektgrupper/arbetsgrupper)
* Alla övriga förtroendeposter som hör till föreningens verksamhet. 

Valberedningen ska söka ledamöter främst till;

* De poster där avhopp skett under året, från ovanstående nämnda poster.
* De ledamöter som står i tur att avgå i styrelsen och för revisor (och andra poster).
Men detta är inte något hinder för att även förslag kommer in på redan tillsatta poster, och dessa förslag måste naturligtvis också redovisas för medlemmarna av valberedningen. 

Det är särskilt viktigt att valberedningen känner till vilka funktioner som ska bemannas. Valberedningen träffar aktuella kandidater och presenterar arbetsuppgifterna. Det är mycket angeläget att tydligt presentera föreningen och dess organisation, arbetssätt och vad uppgiften kräver i form av kunskap, tidsåtgång och engagemang. Det är bra om det finns flera kandidater till respektive post. Valberedningen ska inte begära att kandidaterna måste ta ställning direkt, utan kandidaterna ska i lugn och ro fundera över vad de vill utifrån den information de fått.
           Styrelsekompetens
Som ledning till vilken kompetens som bör finnas i en styrelse kan man tänka sig följande sammansättning;

* Jurist / juridiskt kunnig
* Byggnadsingenjör / byggnadskunnig
* Tekniker / IT- och webbkunnig
* Civilekonom / ekonom
* Samhällsvetare
* Entreprenör
* Informatör, journalist, eller liknande som är duktig på att skriva och informera

Valberedningen bör sträva efter en allsidig bemanning i styrelsen avseende ålder, kön, yrkeserfarenheter och kompetens.
